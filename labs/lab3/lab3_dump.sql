
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `lab3`
--

-- --------------------------------------------------------

--
-- Table structure for table `actormovie`
--

CREATE TABLE `actormovie` (
  `movie_id` int(11) DEFAULT NULL,
  `actor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actormovie`
--

INSERT INTO `actormovie` (`movie_id`, `actor_id`) VALUES
(0, 0),
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `actors`
--

CREATE TABLE `actors` (
  `id` int(11) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `given_names` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actors`
--

INSERT INTO `actors` (`id`, `last_name`, `given_names`, `age`, `status`, `created_at`) VALUES
(0, 'Cotten', 'Joseph', 88, 'active', '2021-04-13 13:23:34'),
(1, 'Dalio', 'Marcel', 83, 'active', '2021-04-13 13:23:34'),
(2, 'Dullea', 'Keir', 84, 'active', '2021-04-13 13:23:34'),
(3, 'Helm', 'Brigitte', 90, 'active', '2021-04-13 13:23:34'),
(4, 'Hunter', 'Holly', 63, 'active', '2021-04-13 13:23:34'),
(5, 'Peltola', 'Markku', 51, 'active', '2021-04-13 13:23:34'),
(6, 'Gaup', 'Mikkel', 53, 'active', '2021-04-13 13:23:34'),
(7, 'Dreyden', 'Sergey', 79, 'active', '2021-04-13 13:23:34'),
(8, 'Maggiorani', 'Lamberto', 73, 'active', '2021-04-13 13:23:34'),
(9, 'Thomsen', 'Ulrich', 57, 'active', '2021-04-13 13:23:34');

-- --------------------------------------------------------

--
-- Table structure for table `directormovies`
--

CREATE TABLE `directormovies` (
  `movie_id` int(11) DEFAULT NULL,
  `director_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `directormovies`
--

INSERT INTO `directormovies` (`movie_id`, `director_id`) VALUES
(0, 0),
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `id` int(11) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `given_names` varchar(255) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`id`, `last_name`, `given_names`, `age`, `status`) VALUES
(0, 'Welles', 'Orson', 70, 'active'),
(1, 'Renoir', 'Jean', 84, 'active'),
(2, 'Kubrick', 'Stanley', 70, 'active'),
(3, 'Lang', 'Fritz', 85, 'active'),
(4, 'Campion', 'Jane', 66, 'active'),
(5, 'Kaurismäki', 'Aki', 64, 'active'),
(6, 'Gaup', 'Nils', 66, 'active'),
(7, 'Sokurov', 'Aleksandr', 69, 'active'),
(8, 'De Sica', 'Vittorio', 73, 'active'),
(9, 'Vinterberg', 'Thomas', 51, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `genremovie`
--

CREATE TABLE `genremovie` (
  `movie_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genremovie`
--

INSERT INTO `genremovie` (`movie_id`, `genre_id`) VALUES
(0, 0),
(0, 1),
(2, 2),
(2, 0),
(1, 2),
(1, 0),
(3, 0),
(3, 4),
(4, 0),
(4, 5),
(4, 6),
(5, 2),
(5, 0),
(5, 6),
(6, 7),
(6, 3),
(6, 0),
(7, 0),
(7, 8),
(7, 9),
(8, 0),
(9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(0, 'Drama'),
(1, 'Mystery'),
(2, 'Comedy'),
(3, 'Adventure'),
(4, 'Sci-Fi'),
(5, 'Music'),
(6, 'Romance'),
(7, 'Action'),
(8, 'Fantasy'),
(9, 'History');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `studio_id` int(11) DEFAULT NULL,
  `description` longtext,
  `number_of_rentals` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `title`, `year`, `studio_id`, `description`, `number_of_rentals`, `created_at`) VALUES
(0, 'Citizen Kane', 1941, 0, 'Following the death of publishing tycoon Charles Foster Kane, reporters scramble to uncover the meaning of his final utterance; \'Rosebud\'.', 110, '2021-04-13 13:34:18'),
(1, 'The Rules of the Game', 1939, 1, 'A bourgeois life in France at the onset of World War II, as the rich and their poor servants meet up at a French chateau.', 77, '2021-04-13 13:34:18'),
(2, 'A Space Odyssey', 1968, 2, 'After discovering a mysterious artifact buried beneath the Lunar surface, mankind sets off on a quest to find its origins with help from intelligent supercomputer H.A.L. 9000.', 55, '2021-04-13 13:34:18'),
(3, 'Metropolis', 1927, 3, 'In a futuristic city sharply divided between the working class and the city planners, the son of the city\'s mastermind falls in love with a working-class prophet who predicts the coming of a savior to mediate their differences.', 99, '2021-04-13 13:34:18'),
(4, 'The Piano', 1993, 4, 'In the mid-19th century, a mute woman is sent to New Zealand along with her young daughter and prized piano for an arranged marriage to a wealthy landowner, but is soon lusted after by a local worker on the plantation.', 79, '2021-04-13 13:34:18'),
(5, 'The Man Without a Past', 2002, 5, 'M arrives in Helsinki only to be viciously attacked by thugs and pronounced dead by medics. He revives but with no memory of his past or his identity. He rebuilds his life from scratch, but the past inevitably catches up with him.', 201, '2021-04-13 13:34:18'),
(6, 'Pathfinder', 1987, 6, 'A Warband intent on slaughter terrorizes northern Scandinavian villages in 1000 AD.', 61, '2021-04-13 13:34:18'),
(7, 'Russian Ark', 2002, 7, 'A 19th century French aristocrat, notorious for his scathing memoirs about life in Russia, travels through the Russian State Hermitage Museum and encounters historical figures from the last 200+ years.', 84, '2021-04-13 13:34:18'),
(8, 'Bicycle Thieves', 1948, 0, 'In post-war Italy, a working-class man\'s bicycle is stolen. He and his son set out to find it.', 48, '2021-04-13 13:34:18'),
(9, 'The Celebration', 1998, 2, 'At Helge\'s 60th birthday party, some unpleasant family truths are revealed.', 361, '2021-04-13 13:34:18');

-- --------------------------------------------------------

--
-- Table structure for table `studios`
--

CREATE TABLE `studios` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studios`
--

INSERT INTO `studios` (`id`, `name`, `country`, `created_at`) VALUES
(0, 'RKO Radio Pictures', 'USA', '2021-04-13 13:01:42'),
(1, 'Nouvelles Éditions de Films (NEF)', 'France', '2021-04-13 13:02:12'),
(2, 'Metro-Goldwyn-Mayer (MGM)', 'UK', '2021-04-13 13:05:52'),
(3, 'Universum Film (UFA)', 'Germany', '2021-04-13 13:05:52'),
(4, 'Jan Chapman Productions', 'Australia', '2021-04-13 13:07:19'),
(5, 'Pandora Filmproduktion', 'Finland', '2021-04-13 13:07:19'),
(6, 'Norsk Film', 'Norway', '2021-04-13 13:09:03'),
(7, 'The Hermitage Bridge Studio', 'Russia', '2021-04-13 13:09:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actormovie`
--
ALTER TABLE `actormovie`
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `actor_id` (`actor_id`);

--
-- Indexes for table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directormovies`
--
ALTER TABLE `directormovies`
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `director_id` (`director_id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genremovie`
--
ALTER TABLE `genremovie`
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `genre_id` (`genre_id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studio_id` (`studio_id`);

--
-- Indexes for table `studios`
--
ALTER TABLE `studios`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actormovie`
--
ALTER TABLE `actormovie`
  ADD CONSTRAINT `actormovie_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`),
  ADD CONSTRAINT `actormovie_ibfk_2` FOREIGN KEY (`actor_id`) REFERENCES `actors` (`id`);

--
-- Constraints for table `directormovies`
--
ALTER TABLE `directormovies`
  ADD CONSTRAINT `directormovies_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`),
  ADD CONSTRAINT `directormovies_ibfk_2` FOREIGN KEY (`director_id`) REFERENCES `directors` (`id`);

--
-- Constraints for table `genremovie`
--
ALTER TABLE `genremovie`
  ADD CONSTRAINT `genremovie_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`),
  ADD CONSTRAINT `genremovie_ibfk_2` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`);

--
-- Constraints for table `movies`
--
ALTER TABLE `movies`
  ADD CONSTRAINT `movies_ibfk_1` FOREIGN KEY (`studio_id`) REFERENCES `studios` (`id`);
