/* A result that shows movie title and actors (two fields), grouped by title. */;

SELECT
movies.title as Title,
GROUP_CONCAT(actors.last_name) as Actors
FROM 
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id 
GROUP BY movies.title;

/* ---------------------------------------------------------------------- */;

/* A result that shows actors and all the movies they’ve acted in (two fields), grouped by actor. */;

SELECT
actors.given_names as Actor,
GROUP_CONCAT(movies.title) as Movies
FROM 
Actors
JOIN movie_actor ON actors.id = movie_actor.actor_id
JOIN movies ON movie_actor.movie_id = movies.id 
GROUP BY actors.given_names;

/* ---------------------------------------------------------------------- */;

/* A result that shows all actors and the number of movies they’ve acted in (two fields), grouped by actor. */;

SELECT
actors.given_names as Name,
count(movies.title) as Movies
FROM 
Actors
JOIN movie_actor ON actors.id = movie_actor.actor_id
JOIN movies ON movie_actor.movie_id = movies.id 
GROUP BY actors.given_names;

/* ---------------------------------------------------------------------- */;

/* A result that shows all studios and all the movies they’ve made in (two fields), grouped by studio. */;

SELECT
studios.name as Studio,
GROUP_CONCAT(movies.title) as Movies
FROM
studios
JOIN movies ON studios.id = movies.studio_id
GROUP BY studios.name;

/* ---------------------------------------------------------------------- */;

/* A result that shows all movies, all actors in each movie, and the number of actors in each movie, ordered by the number of actors in each movie (three fields), grouped by movie. */;

SELECT
movies.title as Title,
GROUP_CONCAT(actors.last_name) as Actors,
count(actors.last_name) as Count
FROM 
movies
JOIN movie_actor ON movies.id = movie_actor.movie_id
JOIN actors ON movie_actor.actor_id = actors.id 
GROUP BY movies.title
ORDER BY count(actors.id) DESC;

/* ---------------------------------------------------------------------- */;

/* A result that shows: movie, year, studio, director(s), actor(s), genre(s), grouped by movie title */;

SELECT movies.title as Movie, 
movies.year as Year, 
studios.name as Studio,
GROUP_CONCAT(DISTINCT directors.last_name) as Directors, 
GROUP_CONCAT(DISTINCT actors.last_name) as Actors, 
GROUP_CONCAT(DISTINCT genres.name) as Genres 
FROM 
movies 
JOIN studios ON movies.studio_id = studios.id 
JOIN movie_director ON movies.id = movie_director.movie_id 
JOIN directors ON directors.id = movie_director.director_id
JOIN movie_actor ON movies.id = movie_actor.movie_id 
JOIN actors ON actors.id = movie_actor.actor_id
JOIN movie_genre ON movies.id = movie_genre.movie_id 
JOIN genres ON genres.id = movie_genre.genre_id
GROUP BY movies.title;

/* ---------------------------------------------------------------------- */;

/* A stored procedure that will execute the previous query */;

delimiter /
DROP PROCEDURE IF EXISTS getMovies;
CREATE PROCEDURE getMovies()
BEGIN
SELECT movies.title as Movie, 
movies.year as Year, 
studios.name as Studio,
GROUP_CONCAT(DISTINCT directors.last_name) as Directors, 
GROUP_CONCAT(DISTINCT actors.last_name) as Actors, 
GROUP_CONCAT(DISTINCT genres.name) as Genres 
FROM 
movies 
JOIN studios ON movies.studio_id = studios.id 
JOIN movie_director ON movies.id = movie_director.movie_id 
JOIN directors ON directors.id = movie_director.director_id
JOIN movie_actor ON movies.id = movie_actor.movie_id 
JOIN actors ON actors.id = movie_actor.actor_id
JOIN movie_genre ON movies.id = movie_genre.movie_id 
JOIN genres ON genres.id = movie_genre.genre_id
GROUP BY movies.title;
END;
/

/* ---------------------------------------------------------------------- */;

/* Make a MySQL dump of your database and stored procedure. */;

mysqldump -u root -p --routines lab4 > lab4_dump.sql