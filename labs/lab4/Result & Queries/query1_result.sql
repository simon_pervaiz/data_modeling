/* A result that shows movie title and actors (two fields), grouped by title. */;

mysql> SELECT
    -> movies.title as Title,
    -> GROUP_CONCAT(actors.last_name) as Actors
    -> FROM 
    -> movies
    -> JOIN movie_actor ON movies.id = movie_actor.movie_id
    -> JOIN actors ON movie_actor.actor_id = actors.id 
    -> GROUP BY movies.title;
+--------------------------+---------------------+
| Title                    | Actors              |
+--------------------------+---------------------+
| 2001: A Space Odyssey    | Dulea,Lockwood,Rain |
| Ad Astra                 | Pitt,Jones          |
| Night of the Living Dead | Jones,Oday          |
| Star wars                | Fisher,Hammil,Ford  |
| The French Connection    | Hackman,Scheider    |
+--------------------------+---------------------+
5 rows in set (0.00 sec)
