/* A result that shows actors and all the movies they’ve acted in (two fields), grouped by actor. */;

mysql> SELECT
    -> actors.given_names as Actor,
    -> GROUP_CONCAT(movies.title) as Movies
    -> FROM 
    -> Actors
    -> JOIN movie_actor ON actors.id = movie_actor.actor_id
    -> JOIN movies ON movie_actor.movie_id = movies.id 
    -> GROUP BY actors.given_names;
+-----------+--------------------------+
| Actor     | Movies                   |
+-----------+--------------------------+
| Bradd     | Ad Astra                 |
| Carrie    | Star wars                |
| Douglas   | 2001: A Space Odyssey    |
| Duane     | Night of the Living Dead |
| Gary      | 2001: A Space Odyssey    |
| Gene      | The French Connection    |
| Harrison  | Star wars                |
| Judith    | Night of the Living Dead |
| Keirr     | 2001: A Space Odyssey    |
| Mark      | Star wars                |
| Roy       | The French Connection    |
| Tommy Lee | Ad Astra                 |
+-----------+--------------------------+
12 rows in set (0.00 sec)