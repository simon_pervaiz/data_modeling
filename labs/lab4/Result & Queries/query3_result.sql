/* A result that shows all actors and the number of movies they’ve acted in (two fields), grouped by actor. */;

mysql> SELECT
    -> actors.given_names as Name,
    -> count(movies.title) as Movies
    -> FROM 
    -> Actors
    -> JOIN movie_actor ON actors.id = movie_actor.actor_id
    -> JOIN movies ON movie_actor.movie_id = movies.id 
    -> GROUP BY actors.given_names;
+-----------+--------+
| Name      | Movies |
+-----------+--------+
| Bradd     |      1 |
| Carrie    |      1 |
| Douglas   |      1 |
| Duane     |      1 |
| Gary      |      1 |
| Gene      |      1 |
| Harrison  |      1 |
| Judith    |      1 |
| Keirr     |      1 |
| Mark      |      1 |
| Roy       |      1 |
| Tommy Lee |      1 |
+-----------+--------+
12 rows in set (0.00 sec)