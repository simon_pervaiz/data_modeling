/* A result that shows all studios and all the movies they’ve made in (two fields), grouped by studio. */;

mysql> SELECT
    -> studios.name as Studio,
    -> GROUP_CONCAT(movies.title) as Movies
    -> FROM
    -> studios
    -> JOIN movies ON studios.id = movies.studio_id
    -> GROUP BY studios.name;
+------------+---------------------------------+
| Studio     | Movies                          |
+------------+---------------------------------+
| Hammer     | Night of the Living Dead        |
| Lucas Film | Ad Astra,The Martian            |
| MGM        | 2001: A Space Odyssey           |
| Universal  | Star wars,The French Connection |
+------------+---------------------------------+
4 rows in set (0.00 sec)