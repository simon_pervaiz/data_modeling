/* A result that shows all movies, all actors in each movie, and the number of actors in each movie, ordered by the number of actors in each movie (three fields), grouped by movie. */;

mysql> SELECT
    -> movies.title as Title,
    -> GROUP_CONCAT(actors.last_name) as Actors,
    -> count(actors.last_name) as Count
    -> FROM 
    -> movies
    -> JOIN movie_actor ON movies.id = movie_actor.movie_id
    -> JOIN actors ON movie_actor.actor_id = actors.id 
    -> GROUP BY movies.title
    -> ORDER BY count(actors.id) DESC;
+--------------------------+---------------------+-------+
| Title                    | Actors              | Count |
+--------------------------+---------------------+-------+
| 2001: A Space Odyssey    | Dulea,Lockwood,Rain |     3 |
| Star wars                | Fisher,Hammil,Ford  |     3 |
| Ad Astra                 | Pitt,Jones          |     2 |
| Night of the Living Dead | Jones,Oday          |     2 |
| The French Connection    | Hackman,Scheider    |     2 |
+--------------------------+---------------------+-------+
5 rows in set (0.00 sec)