/* A result that shows: movie, year, studio, director(s), actor(s), genre(s), grouped by movie title */;

mysql> SELECT movies.title as Movie, 
    -> movies.year as Year, 
    -> studios.name as Studio,
    -> GROUP_CONCAT(DISTINCT directors.last_name) as Directors, 
    -> GROUP_CONCAT(DISTINCT actors.last_name) as Actors, 
    -> GROUP_CONCAT(DISTINCT genres.name) as Genres 
    -> FROM 
    -> movies 
    -> JOIN studios ON movies.studio_id = studios.id 
    -> JOIN movie_director ON movies.id = movie_director.movie_id 
    -> JOIN directors ON directors.id = movie_director.director_id
    -> JOIN movie_actor ON movies.id = movie_actor.movie_id 
    -> JOIN actors ON actors.id = movie_actor.actor_id
    -> JOIN movie_genre ON movies.id = movie_genre.movie_id 
    -> JOIN genres ON genres.id = movie_genre.genre_id
    -> GROUP BY movies.title;
+--------------------------+------+------------+-----------+---------------------+-------------------------+
| Movie                    | Year | Studio     | Directors | Actors              | Genres                  |
+--------------------------+------+------------+-----------+---------------------+-------------------------+
| 2001: A Space Odyssey    | 1967 | MGM        | Kubrick   | Dulea,Lockwood,Rain | Science Fiction,Drama   |
| Ad Astra                 | 2020 | Lucas Film | Gray      | Pitt,Jones          | Science Fiction,Drama   |
| Night of the Living Dead | 1971 | Hammer     | Romero    | Jones,Oday          | Horror,Political        |
| Star wars                | 1975 | Universal  | Lucas     | Fisher,Hammil,Ford  | Science Fiction,Romance |
| The French Connection    | 1972 | Universal  | Friedkin  | Hackman,Scheider    | Thriller                |
+--------------------------+------+------------+-----------+---------------------+-------------------------+
5 rows in set (0.13 sec)
