-- MySQL dump 10.13  Distrib 5.7.30, for osx10.12 (x86_64)
--
-- Host: localhost    Database: datamodeling_project2
-- ------------------------------------------------------
-- Server version	5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Food & Beverages'),(2,'Ecommerce'),(3,'Corporate'),(4,'Press & Media'),(5,'Real Estate'),(6,'Biography'),(7,'Web Apps');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `user_id` (`user_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'This is wonderful design, i love how the animation works',4,19,'2021-04-05 23:35:19','2021-04-05 23:35:19'),(2,'Hi, i am looking to a similar layout for my future website can you please contact me (204) 443-5554',1,10,'2021-04-06 23:35:19','2021-04-06 23:35:19'),(3,'Hey i am suprized how this you managed to keep the website works on both language, my website also need a translation feature can you contact me',3,9,'2021-04-20 23:36:42','2021-04-20 23:36:42'),(4,'You guys are amazing designers, perfect design for a bar and restaurant. I love are you projects for restaurants. I am opening a new restaurant and need your services.',2,16,'2021-04-12 23:36:42','2021-04-20 23:36:42'),(5,'Hi can you contact me i need a quote for my ecommerce website, i have a warehouse and almost 20k product in my inventory.',5,9,'2021-04-12 23:39:51','2021-04-12 23:39:51');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) DEFAULT NULL,
  `budget` float DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `launch_date` date DEFAULT NULL,
  `url` varchar(300) NOT NULL,
  `category_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `category_id` (`category_id`),
  KEY `technology_id` (`technology_id`),
  CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `technology_id` FOREIGN KEY (`technology_id`) REFERENCES `technologies` (`technology_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'Northeast Safety Shoes','Northeast Safety Shoes is an online store for order a safety shoes which is specially development for AT&T Telecom Company.','Northeast Safety Shoes is an online store for order a safety shoes which is specially development for AT&T Telecom Company. The store is specially designed for a particular company employee so they can order a pair of work shoes for them and Northeast Safety Shoes admin will accept and process the order.','nss_header.jpg','nss_intro.mp4',20000,'6 Months','2013-12-09','2013-12-09',2,9,1,'2020-12-07 22:18:31','2020-12-07 22:18:31'),(2,'Ralph Homer','Ralph Homer, CEO & co-founder of Lincoln Hospitality, and Restaurant Secrets Inc., has spent most of his adult life in the United Arab Emirates in F&B, as an operator, trainer, consultant and business leader','Ralph is a businessman with a business portfolio and interests that span the region and beyond. Two of his companies are based in Dubai, while several key franchises, expansions and new ventures in regions that include Saudi Arabia, Monaco, and London.\r\nA Maltese national, with homes in Dubai and London, the lifelong entrepreneur with a global sensibility considers his adopted country home and the beginning of an entrepreneurial journey that has seen him open ground-breaking and commercially successful projects around the world.','rh_header.jpg','rh_intro.mp4',4000,'2 Months','2020-12-15','https://www.ralphhomer.com',6,4,1,'2021-01-04 22:20:45','2021-01-04 22:20:45'),(3,'So Tea Cafe','SÖ Tea serves fresh pots of tea that perfectly complement unique, fresh and creative signature bakes and savory specialties with an Emirati & Japanese twist.','The entire menu is created to complement our pots of tea. Unlike other cafés where food is ordered before drinks, here you are first given the tea menu to choose your tea pot and then handed a food menu. Our teaologists then advise on the perfect food pairing for your freshly brewed tea.','stc_header.jpg','stc_intro.mp4',3500,'2 Months','2018-05-08','https://www.soteacafe.com',1,1,1,'2019-10-14 22:24:15','2019-10-14 22:24:15'),(4,'Restaurant Secrets Inc','Restaurant Secrets Inc are Restaurant consultants in Dubai that provide you with Turn-key solutions to owning and operating successful food outlets.','Restaurant Secrets Inc are Restaurant consultants in Dubai that provide you with Turn-key solutions to owning and operating successful food outlets. From restaurant start up & business plans, concept, location analysis, restaurant design & fit out, restaurant menu engineering and kitchen equipments sourcing, POS and management systems, hiring the right staff & providing you with standard operation manuals to run your business.','rsi_header.jpg','rsi_header.mp4',10000,'4 Months','2010-08-11','https://restaurantsecretsinc.com/',3,10,1,'2011-04-12 22:24:15','2011-04-12 22:24:15'),(5,'The Restaurant Company','The Restaurant Co. works alongside talented chefs, restaurants, and food & beverage companies to leverage interest in their offering, maximise selling potential and communicate their stories to media in purposeful ways.','The Restaurant Co. works alongside talented chefs, restaurants, and food & beverage companies to leverage interest in their offering, maximise selling potential and communicate their stories to media in purposeful ways. Unique in our approach, we operate two divisions to optimise exposure – a PR & Communications arm and a culinary newsroom.','trc_header.jpg','trc_intro.mp4',5000,'3 Months','2020-11-01','https://therestaurantcompany.com',4,4,1,'2020-12-31 22:31:56','2020-12-31 22:31:56'),(6,'Arms & McGregor International Realty','Located in Dubai – the ever-pulsing heart of the international real estate market\r\nArms &McGregor International Realty® is a prestigious real estate agency led by a  dynamic executive team with over a decade thriving experience and acclaimed success in property sales, new development marketing, leasing, and property management services. Innovative, experienced and built on strong business principles, Arms &McGregor International Realty® is perfectly poised to deliver vivid results in today’s relentless changing real estate market.','Located in Dubai – the ever-pulsing heart of the international real estate market\r\nArms &McGregor International Realty® is a prestigious real estate agency led by a  dynamic executive team with over a decade thriving experience and acclaimed success in property sales, new development marketing, leasing, and property management services. Innovative, experienced and built on strong business principles, Arms &McGregor International Realty® is perfectly poised to deliver vivid results in today’s relentless changing real estate market.\r\n\r\n“Our business is beyond properties, our business is people.”\r\n\r\nArms &McGregor International Realty®s real estate agency approach is all-encompassing yet remarkably simple; we dedicate equal attention and resources to every sell, buy or lease, while striving to add value to our clients’ projects by creating new business opportunities.\r\nWe recognize that the common thread in each and every real estate transaction is a client with different needs: exceptional customer service, professional guidance throughout the process, expertise of local and international market, and transparency of information. Arms &McGregor International Realty® embraces these needs and works diligently to cater to them on every level of its business.\r\nUnderstanding that each  transaction is different, we continually develop new and innovative methods and approaches to ensure the best possible results are achieved for all our clients.\r\nWhile we are an absolute results driven agency, these results do not come at the cost of honesty, integrity and exceptional service and customer care.\r\n\r\n','amg_header.mp4','amg_intro.mp4',4000,'3 Months','2015-09-14','https://www.armsmcgregor.com/',5,4,1,'2016-10-16 22:31:56','2016-10-16 22:31:56'),(7,'La Serre Digital Menu','Consisting of the Bistro & Boulangerie on the ground floor, a neighbourhood café which hums from morning to night. And the Restaurant on the first floor, which serves a French menu for lunch & dinner, alongside a chic cocktail bar','Consisting of the Bistro & Boulangerie on the ground floor, a neighbourhood café which hums from morning to night. And the Restaurant on the first floor, which serves a French menu for lunch & dinner, alongside a chic cocktail bar. The Restaurant has two spaces for private functions and group bookings; the Chef’s Table, a beautiful table seating up to 14 which adjoins the kitchen, and the private dining room which seats up to 22.','ls_header.jpg','ls_intro.mp4',1000,'3 Weeks','2019-10-23','https://www.laserre.app',7,5,1,'2019-12-31 22:36:17','2019-12-31 22:36:17'),(8,'Distillery Gastropub. After Dark','Bringing an after-hours vibe to Dubai’s hipster scene, Distillery is a new gastropub and late-night bar.','Bringing an after-hours vibe to Dubai’s hipster scene, Distillery is a new gastropub and late-night bar. A community hangout to come to after work or for a meal &amp; pint with friends on the weekend.','dist_header.jpg','dist_intro.mp4',3000,'2 Months','2019-03-31','https://www.distillerydubai.com',1,4,1,'2019-04-24 22:36:17','2019-04-24 22:36:17'),(9,'Crateen','www.crateen.com is an online shopping store founded on Dec 2013.','www.crateen.com is an online shopping store founded on Dec 2013. we are a wholesaling and retailing supplier for a variety of products such as photography tools, fashion, stationery and customized goods based on client requirements. Our fast delivery and competitive prices makes us the right choice for many distributors & shoppers in UAE.','crt_header.jpg','crt_intro.mp4',10000,'6 Months','2015-06-08','https://www.crateen.com',2,11,1,'2016-10-13 22:47:31','2016-10-13 22:47:31'),(10,'Dabang Kathi & Curry','Dabang! Means daring in Hindi. With kathis exploding with punch & flavour, we promise you a blockbuster journey of taste with that added dash of masala.','Dabang! Means daring in Hindi. With kathis exploding with punch & flavour, we promise you a blockbuster journey of taste with that added dash of masala. Can you handle the heat? Hero up & in true bollywood style banish bland forever. Each menu item is the ultimate scene stealer with drama & full action load of flavours.','dbg_header.jpg','dbg_intro.jpg',3500,'2 Months','2019-07-15','https://www.dabangkathi.com/',1,3,1,'2019-09-16 22:50:35','2019-09-16 22:50:35'),(11,'Lou\'Loua Cafe','Lou‘Loua, which means pearl, is where I bring passion & heritage to my home in Dubai. Our menu is a creative journey of traditional flavours of Middle Eastern cooking with new modern experiences for guests in search of a space with international appeal.','‘My story began in Algeria, where growing up with fresh exotic flavors, free flowing milk & honey marked my love & passion for food. My travels around the world have inspired me more but never let me forget the true joy of authentic home cooking. Lou‘Loua, which means pearl, is where I bring passion & heritage to my home in Dubai. Our menu is a creative journey of traditional flavours of Middle Eastern cooking with new modern experiences for guests in search of a space with international appeal. You will taste some of my own best-loved recipes & some inspired creative dishes. Flavor & authenticity will always take centerstage at Lou‘Loua. Come visit Lou‘Loua, where food is sublime, served in a romantic ambience with welcoming service. At Lou‘Loua, I promise you & your loved ones a rare and elegant experience.’','lou_header.jpg','lou_intro.mp4',3000,'2 Months','2019-05-05','http://www.loulouacafe.com/',1,1,1,'2019-09-09 22:50:35','2019-09-09 22:50:35'),(12,'EmiratesGBC','EmiratesGBC is a non-government organisation that promotes and educates on sustainability in the built environment and is the official Green Building Council for the UAE endorsed by the World Green Building Council.','EmiratesGBC is a non-government organisation that promotes and educates on sustainability in the built environment and is the official Green Building Council for the UAE endorsed by the World Green Building Council. EmiratesGBC currently has around 150 members which represents thousands of individuals interested and involved in Green Building in the UAE and the region. In addition, EmiratesGBC members receive discounts on a number of programmes such as those related to education and training and green building events.\r\n\r\nThe Emirates Green Building Council (EmiratesGBC) was founded in June 2006, and became a full member of the World Green Building Council in September 2006.','egb_header.jpg','egb_intro.mp4',6000,'6 Months','2015-04-05','https://emiratesgbc.org/',3,4,1,'2015-04-13 22:56:43','2015-04-13 22:56:43'),(13,'Bestoon Samad Restaurant','In the early seventies of the last century (Kaka Samad) began his group Restaurants his career with a simple work as a worker in a popular restaurant in the city of Kirkuk in northern Iraq, and because of his love for his work and diligence has become after a short period of time a chief in kebab making that gave to his clients dish featured kebab. This increasing of the number of customers made him to look forward to work in Baghdad the capital of Iraq.','In the early seventies of the last century (Kaka Samad) began his group Restaurants his career with a simple work as a worker in a popular restaurant in the city of Kirkuk in northern Iraq, and because of his love for his work and diligence has become after a short period of time a chief in kebab making that gave to his clients dish featured kebab. This increasing of the number of customers made him to look forward to work in Baghdad the capital of Iraq.In Baghdad he worked in a public restaurant in Alalawi region. After that he tried to have his own restaurant. Actually he did .He bought the restaurant that he worked in (Kabab Arbil restaurant).He did all his best worked very hard to make the restaurant succeed and becomes famous. After its brilliant success and at the beginning of eighty\'s he decided to return to his own first city Kirkuk; there he established his first branch of (Samad Restaurant).\r\n\r\nAt the end of ninety\'s; he returned to Baghdad again and founded a second branch of his series of the group in Almansour region.He decided to open a third branch also in Almansour city under the name of the one of his sons who is called Bestoon Samad and it became (Bestoon Samad Restaurant).At last;but not finally the fourth branch is opened in Arbil which bears the name of Samad Restaurant. In this way Samad Restaurant group has founded. To attained and present what is special and delicious to get its customers satisfaction and compliments that is the reason of its success and this will lead him in the opening of the fifth branch in Dubai by the name of Bestoon Samad Restaurant.','bestoon_header.jpg','bestoon_intro.mp4',3000,'2 Months','2020-08-25','https://www.bestoonsamadrestaurant.com/',1,2,1,'2020-08-27 22:56:43','2020-08-27 22:56:43'),(14,'Hamptons Dubai','Hamptons is inspired by the heritage and lifestyle of the Hamptons in the state of New York, our boutique café invites you to savor an atmosphere of distinct style, charm and elegance in Dubai.','Hamptons is inspired by the heritage and lifestyle of the Hamptons in the state of New York, our boutique café invites you to savor an atmosphere of distinct style, charm and elegance in Dubai. The Hamptons menu was carefully designed to exude sophistication and elegance. We aim to offer much more than your typical café. Our comprehensive breakfast, lunch and dinner selection ensures that there is something for every member of the family. ','hampt_header.jpg','hamp_intro.mp4',4000,'3 Months','2013-03-08','https://hamptonsdubai.com/',1,6,1,'2013-04-04 23:02:30','2013-04-04 23:02:30'),(15,'Side Kick Cafe','The most social, mouthwatering, Insta-worthy foodie concept has hit Dubai bringing loaded fries and custom donut cones to satisfy the growls of both young and old.\r\n         ','The most social, mouthwatering, Insta-worthy foodie concept has hit Dubai bringing loaded fries and custom donut cones to satisfy the growls of both young and old. We are talking the crispiest fries, sweetest cones,\r\nfreshest ingredients, hottest toppings and most drool-worthy signatures. Is your spidey-sense tingling yet\r\n         ','sidek_header.jpg','sidek_intro.mp4',3000,'2 Months','2019-12-02','http://www.sidekickuae.com/',1,1,1,'2019-12-12 23:02:30','2019-12-12 23:02:30'),(16,'Distillery Digital Menu','Bringing an after-hours vibe to Dubai’s hipster scene, Distillery is a new gastropub and late-night bar.','Bringing an after-hours vibe to Dubai’s hipster scene, Distillery is a new gastropub and late-night bar. A community hangout to come to after work or for a meal &amp; pint with friends on the weekend.','distillery_header.jpg','distillery_intro.mp4',1000,'3 Weeks','2020-08-18','https://www.distillerydubai.app/',7,5,1,'2020-08-20 23:08:42','2020-08-20 23:08:42'),(17,'Taikun Dubai','Taikun brings the diverse and eclectic flavours of Asian and fuses them with the cosmopolitan and dynamic vibe of Downtown Dubai. The restaurant, bar and lounge combine to deliver an unrivalled multi-sensory dining experience right in the heart of Dubai.\r\n','Taikun brings the diverse and eclectic flavours of Asian and fuses them with the cosmopolitan and dynamic vibe of Downtown Dubai. The restaurant, bar and lounge combine to deliver an unrivalled multi-sensory dining experience right in the heart of Dubai.\r\n\r\nHidden away far from the hustle and bustle of The Boulevarde is the Garden by Taikun. An experience all on its own with shisha, world class food and entertainment and views of the majestic Burj Khalifa it is the premier outdoor lounge in Downtown Dubai.','taik_header.jpg','taik_intro.mp4',1000,'3 Weeks','2020-12-22','https://taikundubai.app/',7,5,1,'2020-12-31 23:08:42','2020-12-31 23:08:42'),(18,'The Loft','The Loft at Dubai Opera is a laid back venue beautifully located at the top of one of the most iconic architectural building in the UAE, the Dubai Opera. It’s the perfect place to catch the sunset, while gazing at the towering Burj Khalifa & the dancing Dubai fountains while enjoying drinks & food at great prices.','The Loft at Dubai Opera is a laid back venue beautifully located at the top of one of the most iconic architectural building in the UAE, the Dubai Opera. It’s the perfect place to catch the sunset, while gazing at the towering Burj Khalifa & the dancing Dubai fountains while enjoying drinks & food at great prices.\r\n\r\nWith daily events, weekly happenings & the theatre experience right at its doorstep, The Loft has something for everyone.\r\n\r\nWe are is most famously known for our party brunches, sundowner terrace with drinks starting at 35 & the weekly Thursday night party. You could enjoy some great steaks here with creative cocktails at the indoor or outdoor bar or opt for our best seller truffle pizzas, hearty pastas or slow cooked meat dishes. During theatre season, The Loft hosts some of the busiest show parties, pre-theatre dining & post theatre party crowd.\r\n\r\nWith a 350 seater capacity, we’ve hosted corporate events, weddings & even fashion shows over the years.\r\n\r\nConceived around the poetry of an oyster shell, its award-winning design by Alexander & Co in association with Tribe Studio, is a harmony between oceanic architectural references and contemporary design. Each element pays tribute to the colors and tones of the sea, oyster and sea cliff.\r\n\r\n','loft_header.jpg','loft_intro.mp4',3000,'2 Months','2019-06-03','https://www.loftatopera.com/',1,4,1,'2019-06-04 23:12:53','2019-06-04 23:12:53'),(19,'Funoon District','The Funoon District is a brilliantly unique destination where home-grown F&B brands, retailers, entertainment venues & local talent all come together to create an evolving space with many offerings.','The Funoon District is a brilliantly unique destination where home-grown F&B brands, retailers, entertainment venues & local talent all come together to create an evolving space with many offerings. A walk through the lush green grounds reveals streets named after Musical Legends lined with kiosks, food trucks & social connectors. The all year round air-conditioned al fresco dining space offers a variety of restaurant & cafés.','funoon_header.jpg','funoon_intro.mp4',5000,'5 Months','2017-09-01','https://www.funoondistrict.ae/',6,7,1,'2017-09-19 23:12:53','2017-09-19 23:12:53'),(20,'OPSO Dubai','Opso is a modern Mediterranean restaurant inspired by modern and contemporary tastes focusing on social style dining in a lively atmosphere.','Opso is a modern Mediterranean restaurant inspired by modern and contemporary tastes focusing on social style dining in a lively atmosphere. Opso Dubai added value is the result of its appreciation for the Emirati warm hospitality and passion for excellent food. The Dubai chapter in Opso’s ongoing history is a combination of traditional Mediterranean -Greek cuisine with contemporary flavors in an improved luxury setting that meets Dubai’s expectations and luxurious lifestyle.','opso_header.jpg','opso_intro.mp4',3000,'2 Months','2019-03-06','https://opso.ae',1,6,1,'2019-03-16 23:20:49','2019-03-16 23:20:49');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `technologies`
--

DROP TABLE IF EXISTS `technologies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `technologies` (
  `technology_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`technology_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `technologies`
--

LOCK TABLES `technologies` WRITE;
/*!40000 ALTER TABLE `technologies` DISABLE KEYS */;
INSERT INTO `technologies` VALUES (1,'PHP'),(2,'MySQL'),(3,'HTML5'),(4,'WordPress'),(5,'Ionic'),(6,'CSS'),(7,'jQuery'),(8,'JavaScript'),(9,'CodeIgniter'),(10,'Ruby on Rails'),(11,'Shopify');
/*!40000 ALTER TABLE `technologies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `given_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `gender` enum('m','f') NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Joshua','Lamar','joshua_lamar','joshalamar@themattstudio.com','63d31762a50f937b535746c9e31fa33e','(204) 222-3444','7 St. Thomas Street - Suite 501\r\n','Toronto','M5S 2C3','m',1,'2021-04-04 23:23:39','2021-04-04 23:23:39'),(2,'Simon','Pervaiz','simon.pervaiz433','simon@themattstudio.com','63d31762a50f937b535746c9e31fa33e','(204) 297-3333','500 Sterling Lyon','Winnipeg','R3P 1E7','m',1,'2021-04-13 23:23:39','2021-04-13 23:23:39'),(3,'Salvishia','Simon','salvishia4343','salvishia@themattstudio.com','e6f9dc0e0f4bec1a72ae8c999875e8e1','(204) 444-4443','Festival Plaza Mall Jebel Ali','Dubai','00000','f',1,'2021-04-12 23:29:17','2021-04-12 23:29:17'),(4,'Angelica','John','angeli4323','angelica@themattstudio.com','94a6ba4c8b718e86fd8ae74e936cc097','(204) 232-4222','194, Umm Seqeum, Hessa Street','Dubai','44395','f',1,'2021-04-05 23:29:17','2021-04-05 23:29:17'),(5,'June','Cada','june_cada','june@themattstudio.com','718b6dd54c8d1d3ad19eb99cb12f13e2','(204) 333-5564','Office 23, Oasis Mall, Al Quoz 4','Dubai','45643','m',1,'2021-04-13 23:33:30','2021-04-13 23:33:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-15 23:46:38
